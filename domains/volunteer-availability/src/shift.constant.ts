export const SHIFT_HOURS_UTC = {
  DAY: 8,
  NIGHT: 4,
  PARTY: 16,
};
