export const ONE_MINUTE_IN_MS = 1000 * 60;
export const QUARTER_IN_MS = ONE_MINUTE_IN_MS * 15;
export const ONE_HOUR_IN_MS = ONE_MINUTE_IN_MS * 60;
export const TWO_HOURS_IN_MS = 2 * ONE_HOUR_IN_MS;
export const ONE_DAY_IN_MS = ONE_HOUR_IN_MS * 24;
