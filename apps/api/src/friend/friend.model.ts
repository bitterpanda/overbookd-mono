type Friend = {
  id: number;
  lastname: string;
  firstname: string;
  nickname: string;
};

export type FriendWithData = {
  friend: Friend;
};
