import { FaIdResponse } from '../faTypes';

export class FollowingFaResponseDto implements FaIdResponse {
  id: number;
}
