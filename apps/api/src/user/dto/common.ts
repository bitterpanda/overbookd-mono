export const upperCaseCharacter = new RegExp('[A-Z]+');
export const OneNumber = new RegExp('[\\d]+');

export enum Years {
  A1 = 'A1',
  A2 = 'A2',
  A3 = 'A3',
  A4 = 'A4',
  A5 = 'A5',
  VIEUX = 'VIEUX',
  AUTRE = 'AUTRE',
}
export enum Departments {
  TC = 'TC',
  IF = 'IF',
  GE = 'GE',
  GM = 'GM',
  GI = 'GI',
  GCU = 'GCU',
  GEN = 'GEN',
  SGM = 'SGM',
  BS = 'BS',
  FIMI = 'FIMI',
  AUTRE = 'AUTRE',
}
