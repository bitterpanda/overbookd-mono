import { FaStatus } from '../../fa/fa.model';

export interface Animation {
  id: number;
  name: string;
  status: FaStatus;
}
