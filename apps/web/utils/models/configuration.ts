export type SgConfig = {
  prixFutBlonde: number;
  prixFutBlanche: number;
  prixFutTriple: number;
  prixFutFlower: number;
};

export interface Configuration {
  key: string;
  value: any;
}
