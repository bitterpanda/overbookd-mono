export interface SnackNotif {
  message: string;
  timeout?: number;
  id?: number;
}
