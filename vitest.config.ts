
import { resolve } from 'path'

export default {
  resolve: {
    alias: {
      '@overbook/list': resolve('./libraries/list/src'),
      '@overbook/period': resolve('./libraries/period/src'),
    }
  }
}
